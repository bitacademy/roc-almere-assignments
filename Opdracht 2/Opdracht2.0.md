# Opdracht 2 PHP in the terminal

_Deze opdrachten vinden allemaal plaats in je terminal._

_Voor deze opdrachten is slechts het gebruik van de standard PHP library toegestaan_

Schrijf een klein script dat de wereld groet.

Zie hieronder de input en verwachtte output daarbij.

**Input ($> staat voor normal user, en hoort dus niet bij de input):**
```
$> ./hw.php
```

**Output:**
```Hello World```