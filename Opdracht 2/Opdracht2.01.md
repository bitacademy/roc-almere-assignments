# Opdracht 2.01 Odd Even

Schrijf een programma dat de user vraagt om input. Het programma controleert vervolgens of het opgegeven getal een even of oneven getal is.

Zorg ervoor dat je uit de input komt door middel van CTRL+D.

De readline library is geen onderdeel van de standard PHP library.

**Input & Output:**
```
$> ./oddeven.php
Enter a number: 42
The number 42 is even
Enter a number: 0
The number 0 is even
Enter a number:
'' is not a number
Enter a number: toto
'toto' is not a number
Enter a number: 21
The number 21 is odd
Enter a number: 99cosmos
'99cosmos' is not a number
Enter a number: ^D
```