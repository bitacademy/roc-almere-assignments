# Opdracht 2.02 Split String

Bij het aanroepen van je split string functie, geef je een aantal woorden in een string mee. Deze woorden worden vervolgens losgekoppeld en op alfabetische volgorde gesorteerd, vanaf de eerste spatie die ze bevatten.

Maak 2 files aan, split_string.php en main.php. Deze code dient in main.php te staan

```
<?PHP
include("split_string.php");
print_r(splitString("Hello &nbsp;&nbsp;&nbsp; World AAA")); ?>
```

**Input & Output:**
```
$> ./main.php
Array
(
[0] => AAA
[1] => Hello
[2] => World
)
```