# Opdracht 2.03 IsSorted

Schrijf een functie die checkt of een array al gesorteerd is.

```
<?PHP
include("is_sorted.php");
$tab = array("!/@#;^", "42", "Hello World", "hi", "zZzZzZz");
$tab[] = "What are we doing now ?";
if (isSorted($tab))
echo "The array is sorted\n";
else
echo "The array is not sorted\n";
?>
```

**Input & Output:**
```
$> ./main.php
The array is not sorted
```