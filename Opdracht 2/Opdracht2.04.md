# Opdracht 2.04 Simple calculator

Dit PHP script neemt 3 arguments. Een getal, een reken operator, en nog een getal. Zorg dat de volgende rekenoperatoren ondersteund worden:
`+`, `-`, `*`, `/`, `%`.

Het programma hoeft geen errors af te handelen, maar dient wel aan te geven wanneer het een onjuist aantal parameters krijgt.

**Input & Output:**
```
$> ./simple_calc.php
Incorrect Parameters
$> ./simple_calc.php 1 + 3
4
$> ./simple_calc.php " 1" " +" " 3"
4
$> ./simple_calc.php " 1" " *" " 3"
3
$> ./simple_calc.php 42 "% " 3
0
```