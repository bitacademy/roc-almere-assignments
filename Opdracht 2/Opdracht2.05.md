# Opdracht 2.05 Simple calculator 2

Je gaat je rekenmachine ombouwen. In plaats van meerdere arguments, neemt hij er nu slechts 1. De berekening is dus altijd in hetvolgende format: _getal_ _operator_ _getal_. Als er niet aan deze syntax voldaan wordt, print het programma "Syntax error". Er kunnen geen spaties tussen getallen plaatsvinden, maar ook veel.

**Input & Output:**
```
$> ./do_op_2.php
Incorrect Parameters
$> ./do_op_2.php toto
Syntax Error
$> ./do_op_2.php "42*2"
84
$> ./do_op_2.php "  42 / 2 "
21
$> ./do_op_2.php "six6*7sept"
Syntax Error
$> ./do_op_2.php '`rm -rf ~/`;'
Syntax Error
```