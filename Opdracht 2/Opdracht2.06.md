# Opdracht 2.06 Key Value 

Het doel van dit programma is om het passende key value pair te weergeven dat als eerste argument mee wordt gegeven. Na je gewenste key, kun je oneindig veel key:value pairs meegeven.

**Input & Output:**
```
$> ./search_it!.php
$> ./search_it!.php toto
$> ./search_it!.php toto "key1:val1" "key2:val2" "toto:42"
42
$> ./search_it!.php toto "toto:val1" "key2:val2" "toto:42"
42
$> ./search_it!.php "toto" "key1:val1" "key2:val2" "0:hop"
$> ./search_it!.php "0" "key1:val1" "key2:val2" "0:hop"
hop
$>
```