# Opdracht 3.0 Images

Dit script neemt een parameter: de link van een webpage. Het script haalt alle images van de meegegeven webpage af.

**Input & Output:**
```
$> ls
photos.php
$> ./photos.php "http://www.bit-students.com"
$> ls
photos.php
www.bit-students.com/
$> ls www.bit-students.com/
BIT students.png
Website background.png
All logo's.png
$>
```