# Opdracht 3.1 Base64 image

Maak een page die verificatie op het HTTP niveau vereist. Als je inlogt met username: `BITStudent` en password: `4life`, kom je uit op een pagina wiens source code bestaat uit een img tag met als source het BIT Students logo wat je op de bit-students.com website vindt. Let op, de src van de image dient geen URL te zijn, maar direct het BIT Logo.

Als de user met foutieve credentials probeert in te loggen, weergeef dan een error message.

**Input & Output:**
```
$> curl --user BITStudent:4life http://localhost:8888/Opdracht3.1.php
<html><body>
Hello BITStudent<br />
<img src='data:image/png;base64,iVBORw0KGgoAAAA...
... ...
...kdjAk58AAAAAASUVORK5CYII'>
</body></html>
$>
```