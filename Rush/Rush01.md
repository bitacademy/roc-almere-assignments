# Rush 01 Dead links

In deze opdracht maak je een programma wat dead links opspoort. Allereerst, ga je een sample dataset maken. De dataset bevat urls die OK responses geven en dead links. 

Op de web page dien je de dataset te kunnen uploaden in CSV format. Controleer de dataset op dead links en print vervolgens een tabel waarin je alle dead links uit de dataset weergeeft.

De gebruiker dient de mogelijkheid te bezitten om de originele CSV zonder de dead links te downloaden.